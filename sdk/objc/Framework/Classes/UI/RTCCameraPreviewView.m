/*
 *  Copyright 2015 The WebRTC Project Authors. All rights reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree. An additional intellectual property rights grant can be found
 *  in the file PATENTS.  All contributing project authors may
 *  be found in the AUTHORS file in the root of the source tree.
 */

#import "WebRTC/RTCCameraPreviewView.h"

#import <AVFoundation/AVFoundation.h>
#import <UIKit/UIKit.h>

#import "RTCDispatcher+Private.h"
#import "mydefs.h"

@interface RTCCameraPreviewView()
@property (nonatomic) UIDeviceOrientation lastDeviceOrientation;
@end

@implementation RTCCameraPreviewView

@synthesize captureSession = _captureSession;
@synthesize lastDeviceOrientation = _lastDeviceOrientation;

+ (Class)layerClass {
  return [AVCaptureVideoPreviewLayer class];
}

- (instancetype)initWithFrame:(CGRect)aRect {
  self = [super initWithFrame:aRect];
  if (self) {
    [self addOrientationObserver];
#if PORTRAIT_MODE
    _lastDeviceOrientation = UIDeviceOrientationPortrait;
#else
    _lastDeviceOrientation = UIDeviceOrientationLandscapeRight;
#endif
  }
  return self;
}

- (instancetype)initWithCoder:(NSCoder*)aDecoder {
  self = [super initWithCoder:aDecoder];
  if (self) {
    [self addOrientationObserver];
#if PORTRAIT_MODE
      _lastDeviceOrientation = UIDeviceOrientationPortrait;
#else
      _lastDeviceOrientation = UIDeviceOrientationLandscapeRight;
#endif
  }
  return self;
}

- (void)dealloc {
  [self removeOrientationObserver];
}

- (void)setCaptureSession:(AVCaptureSession *)captureSession {
  if (_captureSession == captureSession) {
    return;
  }
  [RTCDispatcher dispatchAsyncOnType:RTCDispatcherTypeMain
                               block:^{
    _captureSession = captureSession;
    AVCaptureVideoPreviewLayer *previewLayer = [self previewLayer];
    [RTCDispatcher dispatchAsyncOnType:RTCDispatcherTypeCaptureSession
                                 block:^{
      previewLayer.session = captureSession;
      [RTCDispatcher dispatchAsyncOnType:RTCDispatcherTypeMain
                               block:^{
        [self setCorrectVideoOrientation];
      }];
    }];
  }];
}

- (void)layoutSubviews {
  [super layoutSubviews];

  // Update the video orientation based on the device orientation.
  [self setCorrectVideoOrientation];
}

-(void)orientationChanged:(NSNotification *)notification {
  [self setCorrectVideoOrientation];
}

- (void)setCorrectVideoOrientation {
  AVCaptureVideoPreviewLayer *previewLayer = [self previewLayer];

  // First check if we are allowed to set the video orientation.
  if (previewLayer.connection.isVideoOrientationSupported) {
    // Set the video orientation based on device orientation.
#ifdef PORTRAIT_MODE
    if (_lastDeviceOrientation == UIDeviceOrientationPortrait) {
        previewLayer.connection.videoOrientation = AVCaptureVideoOrientationPortrait;
    } else if (_lastDeviceOrientation == UIDeviceOrientationPortraitUpsideDown) {
        previewLayer.connection.videoOrientation = AVCaptureVideoOrientationPortraitUpsideDown;
    } else if (_lastDeviceOrientation == UIDeviceOrientationLandscapeLeft) {
        previewLayer.connection.videoOrientation = AVCaptureVideoOrientationLandscapeLeft;
    } else if (_lastDeviceOrientation == UIDeviceOrientationLandscapeRight) {
        previewLayer.connection.videoOrientation = AVCaptureVideoOrientationLandscapeRight;
    }
#else
  // Get current device orientation.
  UIDeviceOrientation deviceOrientation = [UIDevice currentDevice].orientation;
  
  if (deviceOrientation == UIInterfaceOrientationPortraitUpsideDown) {
      previewLayer.connection.videoOrientation = AVCaptureVideoOrientationPortraitUpsideDown;
  } else if (deviceOrientation == UIInterfaceOrientationLandscapeRight) {
      previewLayer.connection.videoOrientation = AVCaptureVideoOrientationLandscapeRight;
  } else if (deviceOrientation == UIInterfaceOrientationLandscapeLeft) {
      previewLayer.connection.videoOrientation = AVCaptureVideoOrientationLandscapeLeft;
  } else if (deviceOrientation == UIInterfaceOrientationPortrait) {
      previewLayer.connection.videoOrientation = AVCaptureVideoOrientationPortrait;
  }
#endif
  }
}

#pragma mark - Private

- (void)addOrientationObserver {
  [[NSNotificationCenter defaultCenter] addObserver:self
                                            selector:@selector(orientationChanged:)
                                                name:UIDeviceOrientationDidChangeNotification
                                              object:nil];
}

- (void)removeOrientationObserver {
  [[NSNotificationCenter defaultCenter] removeObserver:self
                                                  name:UIDeviceOrientationDidChangeNotification
                                                object:nil];
}

- (AVCaptureVideoPreviewLayer *)previewLayer {
  return (AVCaptureVideoPreviewLayer *)self.layer;
}

@end
