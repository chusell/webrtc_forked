#!/bin/sh
GEN_DIR="."

#target
target_arch="arm64"
additional_target_cpus="[\"arm\"]"

#options
libvpx_build_vp9=false
use_bitcode=false
is_debug=false
include_test=false
code_signing=false

# create args
GN_ARGS="target_os=\"ios\" use_xcode_clang=true is_component_build=false"

#framework build args
#GN_ARGS="${GN_ARGS} enable_stripping=true"

#target_cpu args
GN_ARGS="${GN_ARGS} target_cpu=\"${target_arch}\""
#GN_ARGS="${GN_ARGS} additional_target_cpus=${additional_target_cpus}"

#options args
GN_ARGS="${GN_ARGS} rtc_libvpx_build_vp9=${libvpx_build_vp9}"
GN_ARGS="${GN_ARGS} enable_ios_bitcode=${use_bitcode}"
GN_ARGS="${GN_ARGS} is_debug=${is_debug}"
GN_ARGS="${GN_ARGS} rtc_include_tests=${include_test}"
GN_ARGS="${GN_ARGS} ios_enable_code_signing=${code_signing}"
#GN_ARGS="${GN_ARGS} enable_dsyms=true"

echo "gn gen --args='${GN_ARGS}'"

~/depot_tools/gn gen ${GEN_DIR} --args="${GN_ARGS}"