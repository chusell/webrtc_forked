//
//  vadStatus.h
//  sources
//
//  Created by aki on 2018. 8. 24..
//
#ifndef VadStatus_H
#define VadStatus_H

#include <memory>

#include "rtc_base/messagehandler.h"
#include "rtc_base/thread.h"
#include "system_wrappers/include/clock.h"

//
// SpeechDetectorCallback
//
class SpeechDetectorCallback {
public:
  virtual void onChangeActive(bool active) = 0;
protected:
  virtual ~SpeechDetectorCallback() {}
};

//
// Status enum
//
enum Status {
    kPassive,
    kRequestSending,
    kAllow,
    kDisallow,
};

//
// VadStatus
//
class VadStatus
//  : public rtc::MessageHandler
{
public:
  static VadStatus* Instance();
  
  // vad module
  void setVadActive(bool active);
    
  // speech detector module
  void setAllow(bool allow);
  bool isAllowSendVideo();
  void registerCallback(SpeechDetectorCallback* callback);
   
private:
  VadStatus();
  static VadStatus* _inst;
  
  // vad module
  bool audioActivate_;
  int activeCount_;
  int passiveCount_;
  int64_t lastCheckTime_;
  webrtc::Clock* clock_;
  
  // speech detector module
  void setStatus(Status status);
  Status status_;
  SpeechDetectorCallback* callback_;
  
  std::unique_ptr<rtc::Thread> thread_;
//  void OnMessage(rtc::Message* msg) override;
};

#endif /* VideoSenderVad_H */
