//
//  cadStatus.cc
//  sources
//
//  Created by aki on 2018. 8. 24..
//

#include "vadStatus.h"

#include "rtc_base/logging.h"
#include "rtc_base/location.h"

#define VAD_CHECKING_DURATION_IN_ACTIVE 2000 // 1.5 -> 2
#define VAD_RATE_THREOSHOLD_IN_ACTIVE 0.25 // 0.25

#define VAD_CHECKING_DURATION_IN_PASSIVE 200
#define VAD_RATE_THREOSHOLD_IN_PASSIVE 0.80 // 0.4

//enum {
//  MSG_SET_STATUS = 0,
//};

VadStatus* VadStatus::_inst = nullptr;

VadStatus* VadStatus::Instance() {
  if (_inst == nullptr)
    _inst = new VadStatus();
  return _inst;
}

VadStatus::VadStatus() {
  audioActivate_ = false;
  status_ = kPassive;
  
  lastCheckTime_ = 0;
  activeCount_ = 0;
  passiveCount_ = 0;
  
  clock_ = webrtc::Clock::GetRealTimeClock();
  
  thread_ = rtc::Thread::Create();
  thread_->SetName("vadStatusThread", thread_.get());
  thread_->Start();
}

// MARK: - vad module
void VadStatus::setVadActive(bool active) {
  if (active) {
    RTC_LOG(LS_SENSITIVE) << "[VAD] VAD ACTIVE ";
    activeCount_++;
  } else {
    RTC_LOG(LS_SENSITIVE) << "[VAD] VAD NOT ACTIVE";
    passiveCount_++;
  }
  
  int64_t now = clock_->TimeInMilliseconds();
  if (lastCheckTime_ == 0) {
    lastCheckTime_ = now;
  }
  
  // VAD 체크 주기마다, activate rate를 체크
  // active 상태였으면 주기를 길게 가져가고
  // passive 상태였으면 주기를 짧게 가져간다
  double checkingDuration = audioActivate_? VAD_CHECKING_DURATION_IN_ACTIVE : VAD_CHECKING_DURATION_IN_PASSIVE;
  if (now - lastCheckTime_ > checkingDuration) {
    const float activeRate = (float)activeCount_ / (float)(activeCount_ + passiveCount_);
    
    // active 상태였으면, 1.5초 마다 체크하여 25% 이상 말을 하면 active 유지 (말을 오랫동안 쉬어야 PASSIVE)
    // passive 상태였으면, 200ms 마다 체크하여 40% 이상 말을 하면 active 변경 (확실히 말을 할때에만 ACTIVE)
    const float ratioThreshold = audioActivate_? VAD_RATE_THREOSHOLD_IN_ACTIVE : VAD_RATE_THREOSHOLD_IN_PASSIVE;
    if (activeRate > ratioThreshold) {
      if (audioActivate_ == false) {
        // callback 호출
        if (callback_) {
          callback_->onChangeActive(true);
        }
        // requestSending 으로 변경
        setStatus(kRequestSending);
      }
      audioActivate_ = true;
    } else {
      if (audioActivate_ == true) {
        // callback 호출
        if (callback_) {
          callback_->onChangeActive(false);
        }
        // passive 로 변경
        setStatus(kPassive);
      }
      audioActivate_ = false;
    }
    
    RTC_LOG(LS_VERBOSE) << "[VAD] " << checkingDuration << "ms  " << activeRate << " > " << ratioThreshold << "? // " << audioActivate_;
    
    activeCount_ = 0;
    passiveCount_ = 0;
    lastCheckTime_ = now;
  }
}

// MARK: - speech detector module
void VadStatus::setAllow(bool allow) {
  thread_->Invoke<void>(RTC_FROM_HERE, [this, allow] {
    RTC_LOG(LS_INFO) << "[VAD] setAllow() " << allow << " // current status " << status_;

    if (allow) {
      // 서버로부터 비디오 전송 시작 혹은 허가 메세지가 옴
      if (status_ == kRequestSending || status_ == kDisallow) {
        setStatus(kAllow);
      }
    } else {
      // 서버로부터 비디오 전송 중지 혹은 비허가 메세지가 옴
      if (status_ == kRequestSending || status_ == kAllow) {
        setStatus(kDisallow);
      }
    }
  });
}

bool VadStatus::isAllowSendVideo() {
  return (status_ == kAllow);
  
}

void VadStatus::registerCallback(SpeechDetectorCallback* callback) {
  callback_ = callback;
}

void VadStatus::setStatus(Status status) {
  thread_->Invoke<void>(RTC_FROM_HERE, [this, status] {
    RTC_LOG(LS_INFO) << "[VAD] setStatus() " << status_ << " -> " << status;
    status_ = status;
  });
}

//void VadStatus::OnMessage(rtc::Message* msg) {
//  RTC_LOG(LS_INFO) << "[VAD] OnMessage() " << msg;
//}
